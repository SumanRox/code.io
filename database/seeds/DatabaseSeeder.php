<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // This sits on top of all seeders, and is responsible for execution of child seeders
        $this->call(UserSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(CompanySeeder::class);
    }
}
