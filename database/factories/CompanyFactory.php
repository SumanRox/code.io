<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Company;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'phone' => $faker->phoneNumber,
    ];
});
