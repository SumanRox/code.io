<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Company;
use Intervention\Image\Facades\Image;

class CustomersController extends Controller
{
    //
        // Make a function
        public function index()
        {
            //Initialize customer data to variable
            //$customer=Customer::all();
            // And now we shall retun the data

            //$activeCustomers=Customer::active()->get();
            //$inactiveCustomers=Customer::inactive()->get();
            //$companies=Company::all();
            /*
            return view('customers',[
                'client'=>$customer,
                ]);
                */
                //$customers=Customer::all();//This will cause multiple query execution, N+1 Problem!
                $customers=Customer::with('company')->paginate(15);//Eager Loading
                //This will return customer data with the associated Company
                return view('customers.index',compact('customers'));
        }
        public function create()
        {
            $companies=Company::all();
            $customer=new Customer();
            return view('customers.create',compact('companies','customer'));
        }
        public function store(){
            /*
            //Initalize Customer Class object
            $customer=new Customer();
            //Get the value from the request
            $customer->name=request('name');
            $customer->email=request('email');
            $customer->status=request('status');
            //Save the data
            $customer->save();
            */
            //Mass Assignment

            //lets check for authorized user
            $this->authorize('create',Customer::class);
            $customer=Customer::create($this->validateRequest());
            $this->storeImage($customer);
            // Using this keyword, we pass data via reference in our private function
            return redirect('customers');

        }


         //Let's do a Model Route binding, to get data via laravel
         public function show(Customer $customer)
        {
            //The vairable in show() should be same as variable decalred in route  
               return view('customers.show',compact('customer'));
        }

           public function edit(Customer $customer)
           {
            $companies=Company::all();
            return view('customers.edit',compact('customer','companies'));
           }



           public function update(Customer $customer)
           {            
                        // Update the data
                        $customer->update($this->validateRequest());
                        $this->storeImage($customer);
                        // Using this keyword, we pass data via reference in our private function
                        return redirect('customers');
           }
           
           public function destroy(Customer $customer)
           {
            $this->authorize('delete',$customer);
               $customer->delete();
               return redirect('customers');
           }

           private function validateRequest()
           { 
                        //Validate User Input via laravel
                        return request()->validate([
                            'name'=>'required|min:3',
                            //name validation, which checks for empty name field, and miniimum of 3 characters
                            'email'=>'required|email',
                            'status'=>'required',
                            'company_id'=>'required',
                            'image'=>'sometimes|file|image',
                        ]);
           }
           private function storeImage($customer)
           {
               if(request()->has('image'))//Check if the request has any image
               {
                   $customer->update([
                       'image'=>request()->image->store('uploads','public'),
                   ]);
                   $image=Image::make(public_path('storage/'.$customer->image))->fit(300,300);
                   $image->save();
               }
           }
}
