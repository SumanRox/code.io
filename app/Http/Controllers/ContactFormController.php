<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactForMail;

class ContactFormController extends Controller
{
    //
    public function create()
    {
        return view('contact.create');
    }

    public function store()
    {
                         //Validate User Input via laravel
                         $data= request()->validate([
                            'name'=>'required|min:3',
                            //name validation, which checks for empty name field, and miniimum of 3 characters
                            'email'=>'required|email',
                            'msg'=>'required'
                        ]);
        //Sending Mails via laravel
        Mail::to('test@test.com')->send(new ContactForMail($data));

        //Send Feedback to user about message status
        // session()->flash('msg','Thanks for your message. we\'ll be in touch.')
        return redirect('contact')->with('msg','Thanks for your message. We\'ll be in touch.');
    }

}
