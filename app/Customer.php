<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Company;

class Customer extends Model
{
    //
// Fillable Example
    //protected $fillable=['name','email','status'];
    
    // Guarded Example
    protected $guarded=[];//nothing is guarded
    //Making a Scope
    protected $attributes=[
        'status'=>1
    ];
    //making an accessor of status property
    public function getStatusAttribute($attribute)
    {
        return $this->statusOptions()[$attribute];
    }

    public function scopeActive($query)
    {
        return $query->where('status',1);
    }
    public function scopeInactive($query)
    {
        return $query->where('status',0);
    }
    // Create relations with Company
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    public function statusOptions()
    {
        return 
        [
            0=>'Inactive',
            1=>'Active'
        ];
    }
}
