<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //Set safety off
    protected $guarded=[];
    // Create releation with customer class
    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
}
