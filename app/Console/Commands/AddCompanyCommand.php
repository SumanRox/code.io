<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Company;

class AddCompanyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //protected $signature = 'create:company {name} {phone=N/A}';
    protected $signature = 'create:company';
    //Signature is use to identify required parameters for successfull execution
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a Company';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Making an Interactive Command
        $name=$this->ask('[+] Company Name : ');
        $phone=$this->ask('[+] Company Phone : ');
        
        if($this->confirm('Are you Sure to insert "'.$name.'"?')){
            //We will handle the passed argument with this keyword
                 $company=Company::create([
                'name'=>$name,
                'phone'=>$phone,
            ]);
            $this->info('[+] Added : '.$company->name);
        }
        elseif(!$this->confirm('Are you Sure to insert "'.$name.'"?')){
            $this->warn('[-] Not Added : '.$company->name);
        }
        $this->info('Command Executed Successfully!');
    }
}
