@component('mail::message')
#Thankyou for your message

<strong>Name | </strong>{{$data['name']}}
<br>
<strong>Email | </strong>{{$data['email']}}

<strong>Message</strong>

{{$data['msg']}}
@endcomponent
