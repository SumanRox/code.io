
    <strong><label for="Name">Name</label></strong>
    <div class="form-group form-group-sm ">
    <input type="text" name="name" value="{{ old('name') ?? $customer->name }}" class="form-control">
        <label class="pl-2 text-danger">
            {{$errors->first('name')}}
        </label>
    </div>

    <strong><label for="email">Email</label></strong>
    <div class="form-group form-group-sm  ">
        <input type="email" name="email" class="form-control" value="{{ old('email') ?? $customer->email }}">
        <label class="pl-2 text-danger">
            {{$errors->first('email')}}
        </label>
    </div>

    <strong><label for="status">Status</label></strong>
    <div class="form-group form-group-sm  ">
        <select name="status" id="status" class="form-control">
            <option value="" disabled>Select Customer Status</option>
            <option value="1" {{ $customer->status=='Active' ? 'selected' : '' }}>Active</option>
            <!--
                What we are doing here is, 
                We are checking if the value of customer status is active or inactive,
                and then setting the original value of customer status
            -->
            <option value="0" {{ $customer->status=='Inactive' ? 'selected' : '' }}>Inactive</option>
        </select>
        <label class="pl-2 text-danger">
            {{$errors->first('status')}}
        </label>
    </div>

    <strong><label for="company_id">Company</label></strong>
    <div class="form-group form-group-sm">
        <select name="company_id" id="company_id" class="form-control">
            <option value="" disabled>Select Company</option>
            @foreach($companies as $company)
        <option value="{{ $company->id }}" {{ $company->id==$customer->company_id ? 'selected': '' }} >{{ $company->name }}</option>
            @endforeach
        </select>
        <label class="pl-2 text-danger">
            {{$errors->first('company')}}
        </label>
    </div>

    <strong><label for="image">Profile Image</label></strong>
    <div class="form-group form-group-sm">
        <input type="file" name="image" id="image" class="btn btn-success mt-2 mb-2">
         {{$errors->first('image')}}
    </div>
    @csrf
