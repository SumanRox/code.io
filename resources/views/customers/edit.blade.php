@extends('layouts.app')
@section('content')
<h1>Edit Details | {{$customer->name}}</h1>
<p><a href="/customers">Back</a></p>
<form action="/customers/{{$customer->id}}" method="POST" enctype="multipart/form-data">
@method('PATCH')
<!--You need patch method to update-->
@include('customers.form')
<button type="submit" class="btn btn-primary">Update Customers</button>
</form>
@endsection