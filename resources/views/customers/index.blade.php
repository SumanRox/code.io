@extends('layouts.app')
@section('content')
<h1>Customers List</h1>
@can('create', App\Customer::class)
<p><a href="/customers/create">Add Customers</a></p>
@endcan
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Company</th>
            <th scope="col">Company Phone</th>
            <th scope="col">Customer Status</th>
        </tr>
    </thead>
    <tbody>
            @foreach ($customers as $customer)
        <tr>
        <th scope="row">{{$customer->id}}</th>
        <td>
            @can('view', $customer)
            <a href="/customers/{{$customer->id}}">{{$customer->name}}</a>
            @endcan
        @cannot('view', $customer)
        {{$customer->name}}
        @endcannot
        <!--The Customer Id is passed as value to the route-->
        </td>
        <td>{{$customer->email}}</td>
        <td>{{$customer->company->name}}</td>
        <td>{{$customer->company->phone}}</td>
        <td>{{$customer->status}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="row">
    <div class="col-12 d-flex justify-content-center pt-4">
        {{$customers->links()}}
    </div>
</div>
@endsection