@extends('layouts.app')
@section('content')
<h1>Customers Details</h1>
<p><a href="/customers">Back</a></p>
<h4>{{$customer->name}}</h4>
<div class="row">
    <div class="col-1">
        <a href="/customers/{{$customer->id}}/edit" class="btn btn-outline-primary">Edit</a>
    </div>
    @can('delete', $customer)
    <div class="col">
        <form action="/customers/{{$customer->id}}" method="POST">
            @method('DELETE')
            @csrf
            <button type="submit" name="del" class="btn btn-outline-danger">Delete</button>
            </form>
    </div>
    @endcan
</div>
<hr>
<ul>
    <li><strong>Name : </strong>{{$customer->name}}</li>
    <li><strong>Email : </strong>{{$customer->email}}</li>
    <li><strong>Company : </strong>{{$customer->company->name}}</li>
    <li><strong>Status : </strong>{{$customer->status}}</li>
</ul>

@if($customer->image)
<div class="row">
    <div class="col-12">
    <img src="{{ asset('storage/'.$customer->image)}}" alt="{{$customer->name.' Profile Image'}}" class="im-thumbnail">
    </div>
</div>
@endif

@endsection