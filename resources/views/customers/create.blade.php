@extends('layouts.app')

@section('content')
<h1>Customers</h1>
<p><a href="/customers">Back</a></p>
<form action="/customers" method="POST" enctype="multipart/form-data">
@include('customers.form')
<button type="submit" class="btn btn-primary">Add Customers</button>
</form>
@endsection