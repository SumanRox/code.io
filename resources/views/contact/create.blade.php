@extends('layouts.app')

@section('content')
<h1>Contact Us</h1>
<hr>
<form action="{{ route('contact.store') }}" method="POST">
    <label for="name">Name</label>
    <div class="form-group form-group-sm ">
    <input type="text" name="name" value="{{ old('name')}}" class="form-control">
        <label class="pl-2 text-danger">
            {{$errors->first('name')}}
        </label>
    </div>

    <label for="email">E-Mail</label>
    <div class="form-group form-group-sm  ">
        <input type="email" name="email" class="form-control" value="{{ old('email')}}">
        <label class="pl-2 text-danger">
            {{$errors->first('email')}}
        </label>
    </div>
    <label for="msg">Message</label>
    <div class="form-group form-group-sm  ">
        <textarea name="msg" id="msg" cols="30" rows="10" class="textarea form-control">{{ old('msg')}}</textarea>
        <label class="pl-2 text-danger">
            {{$errors->first('msg')}}
        </label>
    </div>
    @csrf
    <button type="submit" class="btn btn-outline-primary">Send Message</button>
</form>

@endsection