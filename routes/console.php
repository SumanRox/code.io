<?php

use Illuminate\Foundation\Inspiring;
use App\Company;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

/* Making Closure Based Command */
/* Custom Clean-Up Command */
Artisan::command('clean:company-cleanup', function(){
    $this->info('Cleaning!');
    Company::whereDoesntHave('customers')//Get Companines which does't have a customer
    ->get()// Get all the data, of customerless compaines
    ->each(function($company){//assign each of them in a variable called $company
        $company->delete();//then delete those company from the database
        $this->warn('Deleted : '.$company->name);//feedback the user as of which compaines are being deleted!
    });
})->describe('Cleans Unused Companies');//This is just description of the command