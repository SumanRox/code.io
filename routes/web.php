<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('about', function () {
    return view('about');
});
Route::view('/','home');

//Using named routes
Route::get('contact','ContactFormController@create')->name('contact.create');
Route::post('contact','ContactFormController@store')->name('contact.store');
Route::view('about','about');

Route::resource('customers','CustomersController')->middleware('auth');
//Route::get('/customers','CustomersController@index');
//Route::get('/customers/create','CustomersController@create');
//Route::get('/customers/{customer}','CustomersController@show');
//Route::get('/customers/{customer}/edit','CustomersController@edit');
//Route::patch('/customers/{customer}','CustomersController@update');
//{{customer}} is acting as a variable to pass the data to the controller
//Route::delete('/customers/{customer}','CustomersController@destroy');

//For form submission use post request
//Route::post('customers','CustomersController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
